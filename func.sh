_sep(){
    echo -e "\n========== $1 ==========\n"
}


get_current_shell(){
    if test -n "$ZSH_VERSION"; then
        echo zsh
    elif test -n "$BASH_VERSION"; then
        echo bash
    else
        echo sh
    fi
}

